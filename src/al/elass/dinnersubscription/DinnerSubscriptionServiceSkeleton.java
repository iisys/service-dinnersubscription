package al.elass.dinnersubscription;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * The skeleton for the DinnerSubscription service.
 * 
 * @author Karim El Assal
 * @version 1.0
 */
public class DinnerSubscriptionServiceSkeleton 
    implements DinnerSubscriptionServiceSkeletonInterface {

    /**
     * Database setting: hostname.
     */
    private static final String HOSTNAME = "127.0.0.1";
    
    /**
     * Database setting: port.
     */
    private static final String PORT = "5432";
    
    /**
     * Database setting: username.
     */
    private static final String USERNAME = "postgres";
    
    /**
     * Database setting: password.
     */
    private static final String PASSWORD = "root";
    
    /**
     * Database setting: database.
     */
    private static final String DATABASE = "soaws";
    
    /**
     * The database connection.
     */
    private static Connection connection;

    /**
     * Get the singleton database connection.
     * @return the database connection resource
     */
    private static Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error loading driver: " + ex);
            }

            String url = "jdbc:postgresql://" + HOSTNAME + ":" + PORT + "/" + DATABASE;

            try {
                connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return connection;
    }

    
    /**
     * Gets the statistics of the last 60 days, per person: amount of dinners attended 
     * (including any extra subscriptions per date), amount of dinners cooked, and the 
     * ratio between those two values.
     * 
     * @param getStatisticsRequest      the request object
     * @return the DinnerStatistics object. See the WSDL specification for the exact 
     *         return object.
     */
    public GetStatisticsResponse getStatistics(GetStatistics getStatisticsRequest) {
        
        DinnerStatistics stats = new DinnerStatistics();
        stats.setFrom(new Date(new Date().getTime() - 1000 * 3600 * 24 * 60L));
        stats.setTo(new Date());
        People people = new People();
        List<Person> personList = new LinkedList<Person>();
        
        try {
            PreparedStatement st = getConnection().prepareStatement(
                  " SELECT    person as name, "
                + "         SUM(attendees) as attended, "
                + "         SUM(willcook::int) as cooked, "
                + "            SUM(willcook::int)::float / SUM(attendees)::float as ratio "
                + " FROM    dinnersubscriptions "
                + " WHERE    dinnerdate >= ? "
                + "   AND    dinnerdate <= ? "
                + " GROUP BY person");
            st.setDate(1, util2sql(stats.getFrom()));
            st.setDate(2, util2sql(stats.getTo()));
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Person person = new Person();
                person.setName(rs.getString("name"));
                person.setDinnersAttended(rs.getInt("attended"));
                person.setDinnersCooked(rs.getInt("cooked"));
                person.setRatio(rs.getFloat("ratio"));
                personList.add(person);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        people.setPerson(personList.toArray(new Person[personList.size()]));
        stats.setPeople(people);
        GetStatisticsResponse response = new GetStatisticsResponse();
        response.setDinnerStatistics(stats);
        
        return response;
    }

    /**
     * Update the dinner subscription of a specific date. 
     * 
     * @param updateSubscription        the request object
     * @return the updated Subscription object
     */
    public UpdateSubscriptionResponse updateSubscription(UpdateSubscription updateSubscription) {
        
        final Subscription subscription = correctSubscription(updateSubscription.getSubscription());
        
        Attendee[] attendee = subscription.getAttendees().getAttendee();
        for (int i = 0; i < attendee.length; i++) {
            if (attendee[i].getPersons() == 0) {
                deleteSubscription(subscription.getDate(), attendee[i].getName());
                continue;
            }
            int updated = updateDbSubscription(subscription.getDate(), attendee[i]);
            if (updated == 0) {
                insertSubscription(subscription.getDate(), attendee[i]);
            }
        }
        
        UpdateSubscriptionResponse response = new UpdateSubscriptionResponse();
        response.setSubscription(subscription);
        return response;
    }

    /**
     * Retrieve the dinner subscriptions of a specific date.
     * 
     * @param getSubscriptionRequest    the request object
     * @return the requested Subscription object
     */
    public GetSubscriptionResponse getSubscription(GetSubscription getSubscriptionRequest) {
        final GetSubscriptionResponse response = new GetSubscriptionResponse();
        final List<Attendee> attendeeList = new LinkedList<Attendee>();
        
        try {
            final PreparedStatement st = getConnection().prepareStatement(
                  " SELECT  * "
                + " FROM     dinnersubscriptions "
                + " WHERE     dinnerdate = ? ");
            st.setDate(1, util2sql(getSubscriptionRequest.getDate()));
            final ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                if (rs.getInt("attendees") == 0) { 
                    continue; 
                }
                final Attendee attendee = new Attendee();
                attendee.setName(rs.getString("person"));
                attendee.setPersons(rs.getInt("attendees"));
                attendee.setWillCook(rs.getBoolean("willcook"));
                attendeeList.add(attendee);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        final Attendees_type0 attendees = new Attendees_type0();
        attendees.setAttendee(attendeeList.toArray(new Attendee[attendeeList.size()]));
        
        final Subscription subscription = new Subscription();
        subscription.setDate(getSubscriptionRequest.getDate());
        subscription.setAttendees(attendees);
        
        response.setSubscription(subscription);
        
        return response;
    }
    
    /**
     * Correct any faulty values in a subscription, such as negative amounts of attendees,
     * true cooking values while there are no attendees, or subscriptions without a person name.
     * 
     * @param old     the old subscription object
     * @return        the new subscription object
     */
    private Subscription correctSubscription(Subscription old) {
        Subscription subscr = new Subscription();
        subscr.setDate(old.getDate());
        List<Attendee> attendeeList = new LinkedList<Attendee>();
        
        if (old.getAttendees().getAttendee() != null) {
            for (Attendee oldAttendee : old.getAttendees().getAttendee()) {
                if (oldAttendee.getName() == null || "".equals(oldAttendee.getName())) {
                    continue;
                }
                
                Attendee newAttendee = new Attendee();
                newAttendee.setName(oldAttendee.getName());
                newAttendee.setPersons(Math.max(0, oldAttendee.getPersons()));
                newAttendee.setWillCook(newAttendee.getPersons() == 0 
                        ? false : oldAttendee.getWillCook());
                attendeeList.add(newAttendee);
            }
        }
        Attendees_type0 attendees = new Attendees_type0();
        attendees.setAttendee(attendeeList.toArray(new Attendee[attendeeList.size()]));
        subscr.setAttendees(attendees);
        
        return subscr;
    }
    
    /**
     * Update a subscription in the database. It assumes all input values are valid.
     * 
     * @param date        the date of the dinner
     * @param attendee    the Attendee instance holding the subscription details
     * @return            the amount of rows updated
     * @throws  RuntimeException if an SQLException was thrown
     */
    private int updateDbSubscription(Date date, Attendee attendee) {
        PreparedStatement st;
        try {
            st = getConnection().prepareStatement(
                  " UPDATE     dinnersubscriptions "
                + " SET     attendees = ?,"
                + "            willcook = ?"
                + " WHERE     dinnerdate = ?"
                + "   AND    person = ?");
            st.setInt(1, attendee.getPersons());
            st.setBoolean(2, attendee.getWillCook());
            st.setDate(3, util2sql(date));
            st.setString(4, attendee.getName());
            st.execute();
            return st.getUpdateCount();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    /**
     * Inserts a new subscription into the database. It assumes all input values are valid.
     * 
     * @param date        the date of the dinner
     * @param attendee    the Attendee instance holding the subscription details
     * @throws RuntimeException if an SQLException was thrown
     */
    private void insertSubscription(Date date, Attendee attendee) {
        PreparedStatement st;
        try {
            st = getConnection().prepareStatement(
                  " INSERT INTO dinnersubscriptions "
                + "            (dinnerdate, person, attendees, willcook) "
                + " VALUES       (?, ?, ?, ?) ");
            st.setDate(1, util2sql(date));
            st.setString(2, attendee.getName());
            st.setInt(3, attendee.getPersons());
            st.setBoolean(4, attendee.getWillCook());
            st.execute();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    /**
     * Deletes a subscription from the database. If a subscription of <code>person</code> 
     * doesn't exist on <code>date</code>, nothing happens.
     * @param date      the date of the subscription in format YYYY-mm-dd
     * @param person    the name of the person
     * @throws RuntimeException if an SQLException was thrown
     */
    private void deleteSubscription(Date date, String person) {
        PreparedStatement st;
        try {
            st = getConnection().prepareStatement(
                  " DELETE FROM dinnersubscriptions "
                + " WHERE         dinnerdate = ?"
                + "   AND        person = ? ");
            st.setDate(1, util2sql(date));
            st.setString(2, person);
            st.execute();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    /**
     * Converts a java.util.Date to a java.sql.Date.
     * @param date      the java.util.Date instance
     * @return          the java.sql.Date instance
     */
    private static java.sql.Date util2sql(Date date) {
        return new java.sql.Date(date.getTime());
    }

}
