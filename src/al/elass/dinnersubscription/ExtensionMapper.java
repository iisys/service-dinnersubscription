
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:04:10 GMT)
 */

        
            package al.elass.dinnersubscription;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://elass.al/DinnerSubscription/".equals(namespaceURI) &&
                  "DinnerStatistics".equals(typeName)){
                   
                            return  al.elass.dinnersubscription.DinnerStatistics.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/DinnerSubscription/".equals(namespaceURI) &&
                  "People".equals(typeName)){
                   
                            return  al.elass.dinnersubscription.People.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/DinnerSubscription/".equals(namespaceURI) &&
                  "Attendees_type0".equals(typeName)){
                   
                            return  al.elass.dinnersubscription.Attendees_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/DinnerSubscription/".equals(namespaceURI) &&
                  "Person".equals(typeName)){
                   
                            return  al.elass.dinnersubscription.Person.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/DinnerSubscription/".equals(namespaceURI) &&
                  "Subscription".equals(typeName)){
                   
                            return  al.elass.dinnersubscription.Subscription.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://elass.al/DinnerSubscription/".equals(namespaceURI) &&
                  "Attendee".equals(typeName)){
                   
                            return  al.elass.dinnersubscription.Attendee.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    