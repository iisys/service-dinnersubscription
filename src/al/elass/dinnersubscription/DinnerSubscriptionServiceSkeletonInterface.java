
/**
 * DinnerSubscriptionServiceSkeletonInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
    package al.elass.dinnersubscription;
    /**
     *  DinnerSubscriptionServiceSkeletonInterface java skeleton interface for the axisService
     */
    public interface DinnerSubscriptionServiceSkeletonInterface {
     
         
        /**
         * Auto generated method signature
         * 
                                    * @param getStatistics
         */

        
                public al.elass.dinnersubscription.GetStatisticsResponse getStatistics
                (
                  al.elass.dinnersubscription.GetStatistics getStatistics
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param updateSubscription
         */

        
                public al.elass.dinnersubscription.UpdateSubscriptionResponse updateSubscription
                (
                  al.elass.dinnersubscription.UpdateSubscription updateSubscription
                 )
            ;
        
         
        /**
         * Auto generated method signature
         * 
                                    * @param getSubscription
         */

        
                public al.elass.dinnersubscription.GetSubscriptionResponse getSubscription
                (
                  al.elass.dinnersubscription.GetSubscription getSubscription
                 )
            ;
        
         }
    